package com.example.kriss.evaluacion_android.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kriss.evaluacion_android.R;




public class Datos_Contacto extends Fragment {

   EditText correo,phone;
   Button btnsig2;

    public Datos_Contacto() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_datos__contacto,container,false);

        correo = (EditText)view.findViewById(R.id.editTextCorreo);
        phone = (EditText)view.findViewById(R.id.editTextTelefono);
        btnsig2 = (Button) view.findViewById(R.id.btnsig2);

        btnsig2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = correo.getText().toString();
                String telefono = phone.getText().toString();

                if(!email.isEmpty()){
                    FragmentTransaction fr = getFragmentManager().beginTransaction();
                    fr.replace(R.id.fragment_container, new Datos_Sesion ());
                    fr.commit();


                }else{
                    Toast.makeText(getActivity(), "el correo el obligatorio", Toast.LENGTH_SHORT).show();
                }


            }
        });



        return view;
    }

}
