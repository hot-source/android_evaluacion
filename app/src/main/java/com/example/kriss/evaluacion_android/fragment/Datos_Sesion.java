package com.example.kriss.evaluacion_android.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kriss.evaluacion_android.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Datos_Sesion extends Fragment {


    EditText usuario,contrasena;
    Button btnsig3;



    public Datos_Sesion() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_datos__sesion,container,false);

        usuario = (EditText)view.findViewById(R.id.editTextUsuario);
        contrasena = (EditText)view.findViewById(R.id.editTextContrasena);
        btnsig3 = (Button)view.findViewById(R.id.btnsig3);

       btnsig3.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               String user = usuario.getText().toString();
               String pass = contrasena.getText().toString();

               if(!user.isEmpty() && !pass.isEmpty()){

                   Toast.makeText(getActivity(), "la Contraseña es :" +pass, Toast.LENGTH_SHORT).show();


               }else{

                   Toast.makeText(getActivity(), "Ambos campos son obligatorios", Toast.LENGTH_SHORT).show();
                   
               }

           }
       });


        return view;
    }

}
