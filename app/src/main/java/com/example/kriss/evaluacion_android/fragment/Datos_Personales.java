package com.example.kriss.evaluacion_android.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kriss.evaluacion_android.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Datos_Personales extends Fragment {


    Button btnsig1;
    EditText nombre,edad;

    public Datos_Personales() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_datos__personales,container,false);

        nombre = (EditText)view.findViewById(R.id.editTextNombre);
        edad = (EditText)view.findViewById(R.id.editTextEdad);
        btnsig1 = (Button) view.findViewById(R.id.btnsig1);

        btnsig1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nombres = nombre.getText().toString();
                String edades =  edad.getText().toString();

                if(!nombres.isEmpty() ){

               FragmentTransaction fr = getFragmentManager().beginTransaction();
               fr.replace(R.id.fragment_container, new Datos_Contacto ());
               fr.commit();

                }else{
                    Toast.makeText(getActivity(), "El nombre es obligatorio", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

}
